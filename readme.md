
step1: install Anaconda3-5.2.0-MacOSX-x86_64


step2: install tensorflow inside virtual environment
```sh
# create virtual environment of python 3.5
conda create -n chatbot python=3.5 anaconda
source activate chatbot
# then install tensorflow inside the virtual environment
pip install tensorflow==1.0.0

```

launch anaconda navigator and choose Application on chatbot
inside anaconda navigator, launch spyder

create a file called chatbot.py

download the data set: cornell movie dialog corpus


///////////////

note: if under windows10, remember to make it not restart in the next 35 days, or all work will need to be redone


